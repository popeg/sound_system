"""
Main file for config. Specify paths, etc.
"""
import os

__author__ = "Przemek"


PROJECT_DIR = os.path.abspath('.')
IMAGES_DIR = os.path.join(PROJECT_DIR, 'resources', 'images')
SOUNDS_DIR = os.path.join(PROJECT_DIR, 'resources', 'sounds')
DEPS_DIR = os.path.join(PROJECT_DIR, 'deps')
FFMPEG_EXE_DIR = os.path.join(DEPS_DIR, 'ffmpeg', 'bin', 'ffmpeg.exe')
# print(PROJECT_DIR)
# print(IMAGES_DIR)
# print(SOUNDS_DIR)
