import os

from pydub import AudioSegment
from pydub.playback import play
from sound_system.config import SOUNDS_DIR, FFMPEG_EXE_DIR


class Sound:
    def __init__(self, sound_name):
        self.path = os.path.join(SOUNDS_DIR, f'{sound_name}')
        self.name, self.extension = os.path.splitext(sound_name)


class SoundSystem:
    def __init__(self):
        self.sound = 0
        self.release = True

        # define sounds in program
        self.kitty_jump = self.load_sound(Sound('alert.wav'))
        self.man_jump = self.load_sound(Sound('Kalimba.mp3'))
        self.arrow_hit = self.load_sound(Sound('alert.wav'))
        self.sms_mp3 = self.load_sound(Sound('sms.mp3'))

    @staticmethod
    def load_sound(sound):
        if sound.extension == '.mp3':
            # todo - handle not relative path, and installation of required libs (so not just os.path)
            AudioSegment.converter = FFMPEG_EXE_DIR
            return AudioSegment.from_mp3(sound.path)
        return AudioSegment.from_wav(sound.path)

    @staticmethod
    def play_sound(sound, limit=5000):
        return play(sound, limit=limit)
