"""
Main module
"""
import pygame as py

from sound_system.system import SoundSystem, Sound

__author__ = "Przemek"


def main():
    py.init()
    images_list = ["sound.png",
                   "mute.png"]
    sounds_list = [Sound('alert.wav'),
                   ]

    # screen = Screen()
    system = SoundSystem()

    # system.play_sound(system.kitty_jump)
    for _ in range(0, 100):
        system.play_sound(system.man_jump)
    # system.play_sound(system.sms_mp3)


if __name__ == "__main__":
    main()
